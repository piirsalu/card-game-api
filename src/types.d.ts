export interface DeckRequest {
  deck_id?: string;
  shuffled?: boolean;
  remaining?: number;
}
