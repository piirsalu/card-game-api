import {Provider} from '@loopback/core';
import {v4 as uuidv4} from 'uuid';

export class UuidProvider implements Provider<String> {
  value() {
    return uuidv4();
  }
}
