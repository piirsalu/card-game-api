import {Client, createRestAppClient, expect} from '@loopback/testlab';
import {CardGameApplication} from '../..';
import {UuidProvider} from '../../services/uuid.service';
import {testdb} from '../fixtures/datasources/testdb.datasource';
import {
  generateCards,
  givenCards,
  givenDeck,
  givenEmptyDatabase,
} from '../helpers/database.helpers';

describe('Deck (acceptance)', () => {
  let app: CardGameApplication;
  let client: Client;

  before(givenEmptyDatabase);
  before(givenRunningApp);
  after(async () => {
    await app.stop();
  });

  it('opens a deck', async () => {
    // arrange
    const deck = await givenDeck({
      id: '49270158-6be4-467e-8154-a367389462c8',
      shuffled: false,
      remaining: 52,
    });

    await givenCards();

    const generatedCards: any[] = generateCards(
      '49270158-6be4-467e-8154-a367389462c8',
    );

    const expected = {
      deck_id: deck.id,
      shuffled: false,
      remaining: 52,
      cards: generatedCards.map(card => {
        return {value: card.value, suit: card.suit, code: card.code};
      }),
    };

    // act
    const response = await client.get(`/decks/${deck.id}/open`);

    // assert
    expect(response.body).to.containEql(expected);
  });

  it('draws a card', async () => {
    // arrange
    const deckId = '49270158-6be4-467e-8154-a367389462c8';

    const expected = {
      cards: [
        {value: '2', suit: 'SPADES', code: '2S'},
        {value: '3', suit: 'SPADES', code: '3S'},
      ],
    };

    const response = await client.get(`/decks/${deckId}/draw-cards/2`);

    // assert
    expect(response.body).to.containEql(expected);
  });

  async function givenRunningApp() {
    app = new CardGameApplication({
      rest: {
        port: 0,
      },
    });
    await app.boot();

    // change to use the test datasource after the app has been booted so that
    // it is not overriden
    app.dataSource(testdb);
    await app.start();
    app.service(UuidProvider, 'uuid');

    client = createRestAppClient(app);
  }
});
