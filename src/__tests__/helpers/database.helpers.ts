import {asyncForEach} from '../../helpers/forEach';
import {Card, Deck} from '../../models';
import {CardRepository, DeckRepository} from '../../repositories';
import {testdb} from '../fixtures/datasources/testdb.datasource';

export async function givenEmptyDatabase() {
  await new CardRepository(testdb).deleteAll();
  await new DeckRepository(testdb).deleteAll();
}

export function givenDeckData(data?: Partial<Deck>) {
  return Object.assign(
    {
      id: '49270158-6be4-467e-8154-a367389462c8',
      shuffled: false,
      remaining: 52,
    },
    data,
  );
}

export function givenCardData(data?: Partial<Card[]>) {
  return generateCards('49270158-6be4-467e-8154-a367389462c8');
}

export function givenDeck(data?: Partial<Deck>) {
  return new DeckRepository(testdb).create(givenDeckData(data));
}

export async function givenCards(data?: Partial<Card[]>) {
  const cards = data ?? givenCardData();
  return asyncForEach(cards, async (card: object) => {
    await new CardRepository(testdb).create(card);
  });
}

export function generateCards(deckId: string): object[] {
  const suits = ['SPADES', 'CLUBS', 'DIAMONDS', 'HEARTS'];
  const ranks = [
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    'JACK',
    'QUEEN',
    'KING',
    'ACE',
  ];

  const cards: object[] = [];

  for (const s in suits) {
    for (const r in ranks) {
      const value = ranks[r];
      const suit = suits[s];
      const code = value.charAt(0) + suit.charAt(0);
      cards.push({
        value,
        suit,
        deckId,
        code,
      });
    }
  }

  return cards;
}
