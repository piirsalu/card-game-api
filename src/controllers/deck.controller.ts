import {inject, intercept, Interceptor} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  get,
  HttpErrors,
  param,
  post,
  Request,
  requestBody,
  RestBindings,
} from '@loopback/rest';
import {asyncForEach, parallelForEach} from '../helpers/forEach';
import {isUuid} from '../helpers/uuid';
import {Card, Deck} from '../models';
import {CardRepository, DeckRepository} from '../repositories';
import {DeckRequest} from '../types';

const validateDeck: Interceptor = async (invocationCtx, next) => {
  if (
    invocationCtx.methodName === 'openDeck' &&
    !isUuidValid(invocationCtx.args[0])
  )
    throw new HttpErrors.InternalServerError('Invalid deck id');
  else if (invocationCtx.methodName === 'drawCards')
    if (!isUuidValid(invocationCtx.args[0]) && !invocationCtx.args[1])
      throw new HttpErrors.InternalServerError(
        'Invalid deck id and card count',
      );
    else if (!isUuidValid(invocationCtx.args[0]) && invocationCtx.args[1])
      throw new HttpErrors.InternalServerError('Invalid deck id');
    else if (isUuidValid(invocationCtx.args[0]) && !invocationCtx.args[1])
      throw new HttpErrors.InternalServerError('Invalid card count');

  const result = await next();
  return result;
};

const isUuidValid = (uuid: string) => {
  return uuid && isUuid(uuid);
};
export class DeckController {
  constructor(
    @inject(RestBindings.Http.REQUEST) private req: Request,
    @inject('services.uuid') private uuid: string,
    @repository(DeckRepository) protected deckRepo: DeckRepository,
    @repository(CardRepository) protected cardRepo: CardRepository,
  ) {}

  @post('/decks')
  async createDeck(@requestBody() deckRequest: DeckRequest): Promise<Deck> {
    // Why even allow remaining as request body property ?
    // Should the amount of cards generated be limited by that ?
    // That doesn't support the 1st requirement of the deck creation
    const deckData: object = {
      id: deckRequest.deck_id ?? this.uuid,
      shuffled: !!deckRequest.shuffled,
      remaining: deckRequest.remaining ?? 52,
    };

    const newDeck = await this.deckRepo.create(deckData);

    const generatedCards = this.generateCards(newDeck.id);
    await this.saveCards(generatedCards);

    return newDeck;
  }

  @intercept(validateDeck)
  @get('/decks/{id}/open')
  async openDeck(
    @param.path.string('id') id: typeof Deck.prototype.id,
  ): Promise<Object> {
    const deckExists = await this.deckRepo.exists(id);
    if (!deckExists)
      throw new HttpErrors.InternalServerError(
        `Deck with id: ${id} doesn't exist`,
      );

    // Could be implemented from model relations
    return Promise.all([
      this.deckRepo.findById(id),
      this.cardRepo.find({
        where: {deckId: id},
        order: ['id ASC'],
      }),
    ]).then(res => {
      const deck: Deck = res[0];
      const cards: Card[] = res[1];
      return {
        deck_id: deck.id,
        shuffled: deck.shuffled,
        remaining: deck.remaining,
        cards: cards.map(card => {
          return {value: card.value, suit: card.suit, code: card.code};
        }),
      };
    });
  }

  @intercept(validateDeck)
  @get('/decks/{id}/draw-cards/{count}')
  async drawCards(
    @param.path.string('id') id: typeof Deck.prototype.id,
    @param.path.number('count') count: number,
  ): Promise<Object> {
    const deckExists = await this.deckRepo.exists(id);
    if (!deckExists)
      throw new HttpErrors.InternalServerError(
        `Deck with id: ${id} doesn't exist`,
      );

    // Order cards by previousCardId when card shuffling is implemented
    const cards: Card[] = await this.cardRepo.find({
      where: {deckId: id, drawn: false},
      order: ['id ASC'],
      limit: count,
    });

    // restrict this request to only return remaining - less data transferred
    const deck: Deck = await this.deckRepo.findById(id);

    await parallelForEach(cards, async (card: Card) => {
      await this.cardRepo.updateById(card.id, {drawn: true});
    });

    await this.deckRepo.updateById(id, {remaining: deck.remaining - count});

    return {
      cards: cards.map(card => {
        return {value: card.value, suit: card.suit, code: card.code};
      }),
    };
  }

  private generateCards(deckId: string): object[] {
    const suits = ['SPADES', 'CLUBS', 'DIAMONDS', 'HEARTS'];
    const ranks = [
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      'JACK',
      'QUEEN',
      'KING',
      'ACE',
    ];

    const cards: object[] = [];

    for (const s in suits) {
      for (const r in ranks) {
        const value = ranks[r];
        const suit = suits[s];
        const code = value.charAt(0) + suit.charAt(0);
        cards.push({
          value,
          suit,
          deckId,
          code,
        });
      }
    }

    return cards;
  }

  private async saveCards(cards: object[]) {
    // async to maintain the card order
    return asyncForEach(cards, async (card: object) => {
      await this.cardRepo.create(card);
    });
  }
}
