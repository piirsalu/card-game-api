import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    foreignKeys: {
      fk_card_deckId: {
        name: 'fk_card_deckId',
        entity: 'Deck',
        entityKey: 'id',
        foreignKey: 'deckId',
      },
    },
  },
})
export class Card extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
    postgresql: {
      columnName: 'id',
      dataType: 'integer',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO',
    },
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'value',
      dataType: 'text',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO',
    },
  })
  value: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'suit',
      dataType: 'text',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO',
    },
  })
  suit: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'code',
      dataType: 'text',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO',
    },
  })
  code: string;

  @property({
    type: 'boolean',
    required: true,
    default: false,
    postgresql: {
      columnName: 'drawn',
      dataType: 'boolean',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO',
    },
  })
  drawn?: boolean;

  // Reference to the previous card id for card order.
  // This approach is optimal as the amount of operations needed
  // to shuffle the deck would be the lowest.
  // Currently not used and set as null
  @property({
    type: 'number',
    required: false,
    postgresql: {
      columnName: 'previousCardId',
      dataType: 'integer',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'YES',
    },
  })
  previousCardId: number | null;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'deckId',
      dataType: 'uuid',
    },
  })
  deckId: string;

  // @belongsTo(() => Deck)
  // deck: Deck;

  constructor(data?: Partial<Card>) {
    super(data);
  }
}

export interface CardRelations {
  //deck?: DeckWithRelations;
}

export type CardWithRelations = Card & CardRelations;
