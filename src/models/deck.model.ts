import {Entity, model, property} from '@loopback/repository';

@model()
export class Deck extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: 'uuidv4',
    postgresql: {
      columnName: 'id',
      dataType: 'uuid',
      nullable: 'NO',
    },
  })
  id: string;

  @property({
    type: 'boolean',
    required: true,
    postgresql: {
      columnName: 'shuffled',
      dataType: 'boolean',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO',
    },
  })
  shuffled: boolean;

  // I would prefer to actually not even add a remaining property to the model
  // Using the relationship call + .length would be a better alternative
  @property({
    type: 'number',
    required: true,
    postgresql: {
      columnName: 'remaining',
      dataType: 'integer',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'NO',
    },
  })
  remaining: number;

  //@hasMany(() => Card)
  // cards: Card[];

  constructor(data?: Partial<Deck>) {
    super(data);
  }
}

export interface DeckRelations {
  // cards?: CardWithRelations[];
}

export type DeckWithRelations = Deck & DeckRelations;
