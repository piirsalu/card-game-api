import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Card, CardRelations} from '../models';

export class CardRepository extends DefaultCrudRepository<
  Card,
  typeof Card.prototype.id,
  CardRelations
> {
  //public readonly deck: BelongsToAccessor<Deck, typeof Card.prototype.id>;
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    //deckRepositoryGetter: Getter<DeckRepository>,
  ) {
    super(Card, dataSource);

    /* this.deck = this.createBelongsToAccessorFor('deck', deckRepositoryGetter);

    this.registerInclusionResolver('deck', this.deck.inclusionResolver); */
  }
}
