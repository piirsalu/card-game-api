import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Deck, DeckRelations} from '../models';

export class DeckRepository extends DefaultCrudRepository<
  Deck,
  typeof Deck.prototype.id,
  DeckRelations
> {
  /* public readonly cards: HasManyRepositoryFactory<
    Card,
    typeof Deck.prototype.id
  >; */

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    //cardRepositoryGetter: Getter<CardRepository>,
  ) {
    super(Deck, dataSource);
    /* this.cards = this.createHasManyRepositoryFactoryFor(
      'cards',
      cardRepositoryGetter,
    );

    this.registerInclusionResolver('card', this.cards.inclusionResolver); */
  }
}
